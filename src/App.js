import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import './App.css';
import RootComponent from './RootComponent';
import store, { persistor } from './foundation/Store';
// import socket from './feature/socketservice';

// socket.connect('localhost', 7905, "");

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootComponent/>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
