import _ from 'lodash';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import TransportComponent from './feature/transport/TransportComponent';
import PlayQueueComponent from './feature/playqueue/PlayQueueComponent';
import SessionComponent from './feature/session/SessionComponent';
import PropTypes from 'prop-types';
import './App.css';

const stateToClassName = (state) => {
  return (state === 'connected')
    ? "MainContent" : "MainContent DisconnectedBlur";
};

const mapStateToProps = (state) => ({
  socketState: _.get(state, 'status.state', 'disconnected'),
});

class RootComponent extends PureComponent {
  render() {
    return (
      <div className="App">
        <div className={stateToClassName(this.props.socketState)}>
          <TransportComponent/>
          <PlayQueueComponent
            trackCacheKey="play_queue"
            trackQueryDisableCount={true}
            trackQueryMessageName="query_play_queue_tracks"
            trackQueryMessageOptions={{}} />
        </div>
        <SessionComponent/>
      </div>
    );
  }
}

RootComponent.propTypes = {
  socketState: PropTypes.string
};

export default connect(mapStateToProps)(RootComponent);
