export const createAction = (type) => {
  let result = (payload) => ({ type, payload });
  result.getType = () => type;
  result.type = type;
  return Object.freeze(result);
};
