import { applyMiddleware, compose } from 'redux';
import transportMiddleware from '../feature/transport/TransportMiddleware';
import playQueueMiddleware from '../feature/playqueue/PlayQueueMiddleware';
import sessionMiddleware from '../feature/session/SessionMiddleware';
import trackCacheMiddleware from '../feature/trackcache/TrackCacheMiddleware';

const allMiddleware = [
  transportMiddleware,
  trackCacheMiddleware,
  playQueueMiddleware,
  sessionMiddleware
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default composeEnhancers(applyMiddleware(...allMiddleware));
