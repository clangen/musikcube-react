import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import allReducers from './Reducers';
import allMiddleware from './Middleware';

const initialState = { };

const persistedReducer = persistReducer({
  key: 'root',
  whitelist: ['session'],
  storage
}, allReducers);

const store = createStore(persistedReducer, initialState, allMiddleware);

export const dispatch = store.dispatch;
export const persistor = persistStore(store);
export default store;
