import { combineReducers } from 'redux';
import transportReducer from '../feature/transport/TransportReducer';
import { sessionReducer, statusReducer } from '../feature/session/SessionReducer';
import trackCacheReducer from '../feature/trackcache/TrackCacheReducer';

export default combineReducers({
  transport: transportReducer,
  session: sessionReducer,
  status: statusReducer,
  tracks: trackCacheReducer
});
