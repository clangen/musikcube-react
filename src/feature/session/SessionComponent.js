import _ from 'lodash';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { storeSession, connectSession } from './SessionActions';
import './Session.css';

const stateToClassName = {
  'connected': 'Connected',
  'disconnected': 'Disconnected'
};

const mapStateToProps = (state) => ({
  socketState: _.get(state, 'status.state', 'disconnected'),
  session: _.get(state, 'session', { })
});

class SessionComponent extends PureComponent {
  onButtonClick = (dispatch) => {
    let hostname = this.refs.hostname.value;
    let port = parseInt(this.refs.port, 10);
    let password = this.refs.password.value;
    port = _.isNaN(port) ? 7905 : port;
    let session = { hostname, port };

    dispatch(storeSession(session));
    dispatch(connectSession({ ...session, password }));
  }

  render() {
    const className = `Session ${stateToClassName[this.props.socketState]}`;
    return (
      <div className={className}>
        <table>
          <tbody>
            <tr>
              <td className="Label">hostname</td>
              <td><input type="text" ref="hostname" defaultValue={this.props.session.hostname || ""}/></td>
            </tr>
            <tr>
              <td className="Label">port</td>
              <td><input type="text" ref="port" defaultValue={this.props.session.port || ""}/></td>
            </tr>
            <tr>
              <td className="Label">password</td>
              <td><input type="password" ref="password" defaultValue={""}/></td>
            </tr>
            <tr>
              <td colSpan="2" onClick={() => this.onButtonClick(this.props.dispatch)} style={{textAlign: 'right'}}><button>log in</button></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

SessionComponent.propTypes = {
  socketState: PropTypes.string,
  session: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(mapStateToProps)(SessionComponent);
