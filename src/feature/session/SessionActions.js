import { createAction } from '../../foundation/Actions';

export const storeSession = createAction('storeSession');
export const connectSession = createAction('connectSession');
export const sessionConnected = createAction('sessionConnected');
export const sessionDisconnected = createAction('sessionDisconnected');
