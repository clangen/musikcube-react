import { connectSession } from './SessionActions';
import socket from '../socketservice';

const sessionMiddleware = store => next => action => { // eslint-disable-line no-unused-vars
  switch (action.type) {
    case connectSession.type: {
      let { hostname, port, password } = action.payload;
      socket.connect(hostname, port, password);
      break;
    }

    default:
      next(action);
  }
};

export default sessionMiddleware;
