import {
  storeSession,
  sessionConnected,
  sessionDisconnected
} from './SessionActions';

const defaultSessionState = {
  hostname: null,
  port: 7905,
};

const defaultStatusState = {
  state: 'disconnected'
};

export const sessionReducer = (state = defaultSessionState, action) => {
  switch (action.type) {
    case storeSession.type:
      return {
        ...state,
        ...action.payload
      };

    case sessionConnected.type:
      return { ...state, state: 'connected' };

    case sessionDisconnected.type:
      return { ...state, state: 'disconnected' };

    default:
      return state;
  }
};

export const statusReducer = (state = defaultStatusState, action) => {
  switch (action.type) {
    case sessionConnected.type:
      return { ...state, state: 'connected' };

    case sessionDisconnected.type:
      return { ...state, state: 'disconnected' };

    default:
      return state;
  }
};
