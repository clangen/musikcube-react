import SocketService, { events, eventType } from './SocketService';

export default SocketService;
export { events, eventType };
