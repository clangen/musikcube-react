import _ from 'lodash';
import ReconnectingWebSocket from 'reconnecting-websocket';
import EventEmitter from 'eventemitter3';
import {
  sessionConnected,
  sessionDisconnected
} from '../session/SessionActions';

const DEFAULT_TIMEOUT_MS = 7500;

const lazy = { dispatch: null };
let socket;
let messageId = 0;
let deviceId = String(Math.random());
let reconnect = false;
const pending = { };

const dispatch = (action) => {
  if (!lazy.dispatch) {
    lazy.dispatch = require('../../foundation/Store').dispatch;
  }
  return lazy.dispatch(action);
};

export const events = new EventEmitter();

export const eventType = {
  connect: 'connect',
  disconnect: 'disconnect',
  message: 'message',
  error: 'error'
};

export const messageType = {
  request: 'request',
  response: 'response',
  broadcast: 'broadcast',
  timeout: 'timeout',
};

const connected = () =>
  socket && socket.readyState === socket.OPEN;

const disconnected = () =>
  socket && (socket.readyState === socket.CLOSED ||
    socket.readyState === socket.CLOSING);


const onConnected = () => {
  events.emit(eventType.connect);
  dispatch(sessionConnected());
};

const onDisconnected = (event) => {
  if (!reconnect && socket) {
    const oldSocket = socket;
    _.defer(() => close(oldSocket));
  }

  if (socket) {
    reconnect = false;
    events.emit(eventType.disconnect, event);
    dispatch(sessionDisconnected());
  }

  socket = null;
};

const onError = (event) => {
  events.emit(eventType.error, event);
};

const onMessageReceived = (event) => {
  const message = Object.freeze(JSON.parse(event.data));
  const context = pending[message.id];

  if (message.type === messageType.response &&
      message.name === 'authenticate')
  {
    reconnect = true;
    onConnected();
  }
  else {
    if (message.type === messageType.broadcast) {
      notify(message);
    }
    else if (message.type === messageType.response) {
      const callback = context && context.callback;
      notify(message, callback);
    }
  }

  if (context) {
    forget(context);
  }
};

const onTimeout = (messageId) => {
  const context = pending[messageId];
  if (context) {
    fail(context);
    forget(context);
  }
};

const notify = (message, callback) => {
  callback && callback(message);
  events.emit(eventType.message, message);
};

const fail = (context, errorType) => {
  const message = _.cloneDeep(context.message);
  message.type = errorType || messageType.timeout;
  notify(Object.freeze(message), context.callback);
};

const forget = (context) => {
  clearTimeout(context.timeoutId);
  delete pending[context.message.id];
};

const close = (instance) => {
  instance = instance || socket;
  if (socket) {
    instance.close(1000, '', { keepClosed: true });
  }
};

const connect = (host, port, password) => {
  disconnect();

  socket = new ReconnectingWebSocket(`ws://${host}:${port}`);

  socket.onopen = () => {
    request("authenticate", { password });
  };

  socket.onclose = onDisconnected;
  socket.onmessage = onMessageReceived;
  socket.onerror = onError;
};

const disconnect = () => {
  close(socket);
  socket = null;
};

const request = (name, options, callback, timeout) => {
  if (connected()) {
    if (_.isFunction(options)) {
      /* if no options are specified, shift arguments left one */
      timeout = callback;
      callback = options;
      options = { };
    }

    const id = `musikcube-react-${messageId++}`;

    const message = Object.freeze({
      name,
      type: messageType.request,
      id,
      device_id: deviceId,
      options: options || { }
    });

    const context = { callback, message };
    pending[id] = context;

    timeout = _.isNumber(timeout) ? timeout : DEFAULT_TIMEOUT_MS;

    if (timeout > 0) {
      context.timeoutId = setTimeout(() => {
        onTimeout(id);
      }, timeout);
    }

    socket.send(JSON.stringify(message));

    return id;
  }
};

const broadcast = (name, options) => {
  if (connected()) {
    socket.send(JSON.stringify({
      type: messageType.broadcast,
      id: `musikcube-react-${messageId++}`,
      device_id: deviceId,
      options: options || { }
    }));
  }
};

const respond = (message, options) => {
  if (connected()) {
    const response = _.deepClone(message);
    response.options = options;
    socket.send(JSON.stringify(response));
  }
};

const cancel = (messageId) => {
  if (pending[messageId]) {
    forget(pending[messageId]);
  }
};

export default {
  connect,
  connected,
  disconnected,
  disconnect,
  request,
  cancel,
  respond,
  broadcast
};
