import { storeTransportState } from './TransportActions';

const transportReducer = (state = { }, action) => {
  switch (action.type) {
    case storeTransportState.type:
      return action.payload.options;
    default:
      return state;
  }
};

export default transportReducer;
