import {
  storeTransportState,
  refreshTransportState,
  pauseOrResume,
  prevTrack,
  nextTrack,
  stop,
  toggleRepeat,
  toggleMute,
  toggleShuffle,
  volumeUp,
  volumeDown,
  playAtIndex,
} from './TransportActions';
import { dispatch } from '../../foundation/Store';
import socket, {
  events as socketEvents,
  eventType as socketEventType
} from '../../feature/socketservice';

const requestPlaybackOverview = () => {
  socket.request('get_playback_overview', (response) => {
    dispatch(storeTransportState(response));
  });
};

socketEvents.addListener(socketEventType.message, (broadcast) => {
  if (broadcast.name === 'playback_overview_changed') {
    dispatch(storeTransportState(broadcast));
  }
});

socketEvents.addListener(socketEventType.connect, requestPlaybackOverview);

requestPlaybackOverview();

const transportMiddleware = store => next => action => { // eslint-disable-line no-unused-vars
  switch (action.type) {
    case pauseOrResume.type:
      socket.request("pause_or_resume");
      break;
    case refreshTransportState.type:
      requestPlaybackOverview();
      break;
    case prevTrack.type:
      socket.request("previous");
      break;
    case nextTrack.type:
      socket.request("next");
      break;
    case stop.type:
      socket.request("stop");
      break;
    case toggleRepeat.type:
      socket.request("toggle_repeat");
      break;
    case toggleMute.type:
      socket.request("toggle_mute");
      break;
    case toggleShuffle.type:
      socket.request("toggle_shuffle");
      break;
    case volumeUp.type:
      socket.request("set_volume", { relative: "up" });
      break;
    case volumeDown.type:
      socket.request("set_volume", { relative: "down" });
      break;
    case playAtIndex.type:
      socket.request('play_at_index', { index: action.payload.index });
      break;
    default:
      next(action);
  }
};

export default transportMiddleware;
