import { createAction } from '../../foundation/Actions';

export const pauseOrResume = createAction('transportPauseOrResume');
export const nextTrack = createAction('nextTrack');
export const prevTrack = createAction('prevTrack');
export const stop = createAction('stop');
export const toggleRepeat = createAction('toggleRepeat');
export const toggleMute = createAction('toggleMute');
export const toggleShuffle = createAction('toggleShuffle');
export const volumeUp = createAction('volumeUp');
export const volumeDown = createAction('volumeDown');
export const playAtIndex = createAction('playAtIndex');
export const storeTransportState = createAction('storeTransportState');
export const refreshTransportState = createAction('refreshTransportState');
