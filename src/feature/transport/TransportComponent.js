import _ from 'lodash';
import React, { Component, PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { 
  pauseOrResume,
  nextTrack,
  prevTrack,
  stop,
  volumeUp,
  volumeDown,
  toggleRepeat,
  toggleShuffle,
  toggleMute,
} from './TransportActions';
import {
  playButtonTitle,
  formatDuration,
} from './TransportUtil';
import './Transport.css';

const mapStateToProps = (state) => ({
  playingTrack: _.get(state, 'transport.playing_track', { }),
  transportState: _.get(state, 'transport', { })
});

const mapDispatchToProps = (dispatch) => ({
  pauseOrResume: () => dispatch(pauseOrResume()),
  nextTrack: () => dispatch(nextTrack()),
  prevTrack: () => dispatch(prevTrack()),
  volumeUp: () => dispatch(volumeUp()),
  volumeDown: () => dispatch(volumeDown()),
  stop: () => dispatch(stop()),
  toggleRepeat: () => dispatch(toggleRepeat()),
  toggleShuffle: () => dispatch(toggleShuffle()),
  toggleMute: () => dispatch(toggleMute())
});

class Link extends PureComponent {
  render() {
    return (
      <div>
        <span>{this.props.title}: </span>
        <span className="Link" onClick={this.props.onClick}>{this.props.value}</span>
      </div>
    );
  }
}

Link.propTypes = {
  title: PropTypes.string,
  value: PropTypes.string,
  onClick: PropTypes.func
};

class TransportComponent extends Component {
  render() {
    const state = this.props.transportState;
    const track = this.props.playingTrack;

    const playbackState = _.get(state, 'state', 'stopped');
    const duration = formatDuration(_.get(state, 'playing_duration', 0));
    const repeatMode = _.get(state, 'repeat_mode', 'unknown');
    const shuffled = String(_.get(state, 'shuffled', false));
    const muted = String(_.get(state, 'muted', false));
    const volume = `${Math.round(_.get(state, 'volume', 1.0) * 100)}%`;

    let trackMetadata = <div className="MetadataLine">not playing</div>;
    if (playbackState !== 'stopped') {
      const title = _.get(track, 'title', 'unknown');
      const artist = _.get(track, 'album_artist', 'unknown');
      const album = _.get(track, 'album', 'unknown');
      trackMetadata = <div className="MetadataLine">playing <b>{title}</b> by <b>{artist}</b> from <b>{album}</b></div>;
    }

    return (
      <div className="Transport">
        <div className="Title">transport controls</div>
        <div className="Content">
          {trackMetadata}
          <div>
            <button onClick={this.props.volumeDown}>vol-</button>
            <button onClick={this.props.volumeUp}>vol+</button>
            <button onClick={this.props.prevTrack}>prev</button>
            <button onClick={this.props.pauseOrResume}>{playButtonTitle(playbackState)}</button>
            <button onClick={this.props.stop}>stop</button>
            <button onClick={this.props.nextTrack}>next</button>
          </div>
          <table className="TransportInfo">
            <tbody>
              <tr>
                <td>state: <b>{playbackState}</b></td>
                <td>duration: <b>{duration}</b></td>
              </tr>
              <tr>
                <td><Link title="repeat" value={repeatMode} onClick={this.props.toggleRepeat}/></td>
                <td><Link title="shuffled" value={shuffled} onClick={this.props.toggleShuffle}/></td>
              </tr>
              <tr>
                <td>volume: <b>{volume}</b></td>
                <td><Link title="muted" value={muted} onClick={this.props.toggleMute}/></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

TransportComponent.propTypes = {
  pauseOrResume: PropTypes.func,
  nextTrack: PropTypes.func,
  prevTrack: PropTypes.func,
  stop: PropTypes.func,
  volumeUp: PropTypes.func,
  volumeDown: PropTypes.func,
  toggleRepeat: PropTypes.func,
  toggleShuffle: PropTypes.func,
  toggleMute: PropTypes.func,
  playingTrack: PropTypes.object,
  transportState: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(TransportComponent);
