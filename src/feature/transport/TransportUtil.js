import _ from 'lodash';
import { sprintf } from 'sprintf-js';

export const formatDuration = (seconds) => {
  seconds = _.isNumber(seconds) ? seconds : 0;
  let hours = Math.floor(seconds / 3600);
  seconds -= (hours * 3600);
  let minutes = Math.floor(seconds / 60);
  seconds -= (minutes * 60);
  if (hours > 0) {
    return sprintf("%02d:%02d:%02d", hours, minutes, seconds);
  }
  return sprintf("%02d:%02d", minutes, seconds);
};

export const playButtonTitle = (playbackState) => {
  switch (playbackState) {
    case 'playing': return 'pause';
    case 'paused': return 'resume';
    default: return 'play';
  }
};
