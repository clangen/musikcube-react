import _ from 'lodash';
import React, { PureComponent } from 'react';
import { AutoSizer, List, CellMeasurerCache, CellMeasurer } from 'react-virtualized';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './PlayQueue.css';
import { playAtIndex } from '../transport/TransportActions';
import { queryCount, queryWindow } from '../trackcache/TrackCacheActions';
import 'react-virtualized/styles.css';
import socket, {
  events as socketEvents,
  eventType as socketEventType
} from '../socketservice';

const mapStateToProps = (state) => ({
  trackCache: _.get(state, 'tracks', { }),
  playIndex: _.get(state, 'transport.play_queue_position', -1),
  playState: _.get(state, 'transport.state', 'stopped')
});

const playAt = (dispatch, index) => dispatch(playAtIndex({ index }));

class PlayQueueComponent extends PureComponent {
  cellMeasurerCache = new CellMeasurerCache({ minHeight: 45, fixedWidth: true });

  constructor() {
    super();

    this.ensureVisibleLoaded = _.debounce((dispatch, payload) => {
      dispatch(queryWindow(payload));
    }, 250);

    this.socketConnected = function() {
      this.requeryCount(this.props.dispatch);
    }.bind(this);
  }

  getTrackCache = () =>
    _.get(this.props, `trackCache.${this.props.trackCacheKey}`, { });

  renderRow = (props, dispatch, context) => {
    const { index, key, style, parent } = context;
    const cache = this.cellMeasurerCache;

    const trackCache = this.getTrackCache();
    const from = _.get(trackCache, 'range.from', 0);
    const track = _.get(trackCache, 'tracks', [])[index - from];

    const title = <div className="RowTitle">{_.get(track, 'title', '-')}</div>;

    let subtitle;
    if (track) {
      subtitle = <div className="RowSubtitle">{track.album_artist} - {track.album}</div>;
    }

    let cls = (index % 2) === 0 ? "ListItem" : "ListItem Odd";
    if (props.playState !== 'stopped' && props.playIndex === index) {
      cls = "ListItem Playing";
    }

    return (
      <CellMeasurer
        cache={cache}
        columnIndex={0}
        key={key}
        rowIndex={index}
        parent={parent}
      >
        <div style={style} className={cls} onClick={playAt.bind(this, dispatch, index)}>
          <div className="RowNumber">{index + 1}.</div>
          <div>
            {title}
            {subtitle}
          </div>
        </div>
      </CellMeasurer>
    );
  }

  requeryCount = (dispatch) => {
    if (this.props.trackQueryDisableCount || !socket.connected()) {
      return;
    }

    const options = this.props.trackQueryMessageOptions || { };

    dispatch(queryCount({
      key: this.props.trackCacheKey,
      request: {
        name: this.props.trackQueryMessageName,
        options: { ...options }
      }
    }));
  }

  rowsRendered = (dispatch, context) => {
    const options = this.props.trackQueryMessageOptions || { };
    this.ensureVisibleLoaded(dispatch, {
      key: this.props.trackCacheKey,
      request: {
        name: this.props.trackQueryMessageName,
        options: {
          ...options,
          offset: context.startIndex,
          limit: (context.stopIndex - context.startIndex) + 1
        }
      }
    });
  }

  componentWillMount() {
    socketEvents.addListener(socketEventType.connect, this.socketConnected.bind(this));
    this.requeryCount(this.props.dispatch);
  }

  componentWillReceiveProps() {
    socketEvents.removeListener(this.socketConnected);
    this.cellMeasurerCache.clearAll();
  }

  render() {
    const renderCache = this.cellMeasurerCache;
    const count = this.getTrackCache().count || 0;

    return (
      <div className="PlayQueue">
        <div className="SectionTitle">play queue</div>
        <div className="List">
          <AutoSizer>
            {({ height, width }) => (
              <List
                rowCount={count}
                rowRenderer={this.renderRow.bind(this, this.props, this.props.dispatch) }
                onRowsRendered={this.rowsRendered.bind(this, this.props.dispatch)}
                deferredMeasurementCache={renderCache}
                rowHeight={renderCache.rowHeight}
                height={height}
                width={width}
              />
            )}
          </AutoSizer>
        </div>
      </div>
    );
  }
}

PlayQueueComponent.propTypes = {
  dispatch: PropTypes.func,
  trackCache: PropTypes.object,
  playIndex: PropTypes.number,
  playState: PropTypes.string,
  trackCacheKey: PropTypes.string,
  trackQueryMessageName: PropTypes.string,
  trackQueryMessageOptions: PropTypes.object,
  trackQueryDisableCount: PropTypes.bool
};

export default connect(mapStateToProps)(PlayQueueComponent);
