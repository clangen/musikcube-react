
import _ from 'lodash';
import { storeCount, invalidateWindow } from '../trackcache/TrackCacheActions';
import { refreshTransportState } from '../transport/TransportActions';
import socket, {
  events as socketEvents,
  eventType as socketEventType
} from '../socketservice';
import { dispatch } from '../../foundation/Store';

export const CACHE_KEY = 'play_queue';

const requeryPlayQueue = () => {
  socket.request('query_play_queue_tracks', { countOnly: true }, (response) => {
    const count = _.get(response, 'options.count', -1);
    if (count >= 0) {
      dispatch(storeCount({ key: CACHE_KEY, count }));
      dispatch(invalidateWindow({ key: CACHE_KEY }));
    }
  });
};

socketEvents.addListener(socketEventType.message, (broadcast) => {
  if (broadcast.name === 'play_queue_changed') {
    dispatch(refreshTransportState());
    requeryPlayQueue();
  }
});

socketEvents.addListener(socketEventType.connect, requeryPlayQueue);

const playQueueMiddleware = store => next => action => { // eslint-disable-line no-unused-vars
  switch (action.type) {
    default: {
      next(action);
      break;
    }
  }
};

export default playQueueMiddleware;
