import { createAction } from '../../foundation/Actions';

export const queryCount = createAction('trackCacheQueryCount');
export const queryWindow = createAction('trackCacheQueryWindow');
export const storeCount = createAction('trackCacheStoreCount');
export const storeWindow = createAction('trackCacheStoreWindow');
export const deleteWindow = createAction('trackCacheDeleteWindow');
export const invalidateWindow = createAction('trackCacheInvalidateWindow');
