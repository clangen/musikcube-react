import {
  storeWindow,
  storeCount,
  deleteWindow
} from './TrackCacheActions';

const emptyValue = {
  range: { from: 0, to: 0 },
  tracks: []
};

const trackCacheReducer = (state = { lastUpdated: 0 }, action) => {
  switch (action.type) {
    case storeCount.type: {
      const key = action.payload.key;
      const count = action.payload.count;
      const value = state[key] || { };

      if (value.count === count) {
        return state; /* no change */
      }

      const lastUpdated = Date.now();

      return {
        ...state,
        lastUpdated,
        [key]: {
          ...emptyValue,
          lastUpdated,
          count,
        }
      };
    }

    case storeWindow.type: {
      const key = action.payload.key;
      const value = state[key];

      if (!state[key]) {
        return state; /* doesn't exist. removed? */
      }

      const lastUpdated = Date.now();
      const tracks = action.payload.data || [];
      const offset = action.payload.offset || 0;

      return {
        ...state,
        lastUpdated,
        [key]: {
          ...value,
          lastUpdated,
          request: action.payload.request,
          tracks: tracks,
          range: {
            from: offset,
            to: offset + tracks.length
          }
        }
      };
    }

    case deleteWindow.type: {
      const key = action.payload.key;

      const without = {
        ...state,
        lastUpdated: Date.now()
      };

      delete without[key];
      return without;
    }

    default:
      return state;
  }
};

export default trackCacheReducer;
