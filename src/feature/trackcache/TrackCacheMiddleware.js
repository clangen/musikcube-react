import _ from 'lodash';
import {
  queryCount,
  storeCount,
  queryWindow,
  storeWindow,
  invalidateWindow
} from './TrackCacheActions';
import socket from '../socketservice';
import { dispatch } from '../../foundation/Store';

let countRequests = { };
let trackRequests = { };

const countTracks = (payload) => {
  const key = payload.key;
  const { name, options } = payload.request;
  const countOnly =  { ...options, countOnly: true };

  if (countRequests[key]) {
    socket.cancel(countRequests[key]);
  }

  countRequests[key] = socket.request(name, countOnly, (response) => {
    const count = _.get(response, 'options.count', -1);
    if (count >= 0) {
      if (countRequests[key] === response.id) {
        delete countRequests[key];
        dispatch(storeCount({ key, count }));
      }
    }
  });
};

const queryTracks = (payload) => {
  const key = payload.key;
  const { name, options } = payload.request;

  if (trackRequests[key]) {
    socket.cancel(trackRequests[key].messageId);
  }

  /* give ourselves a bit of a buffer to try and avoid unnecessary
  requeries */
  options.offset = Math.max(0, options.offset - 10);
  options.limit += 30;

  const from = options.offset;
  const to = from + (options.limit);

  trackRequests[key] = {
    range: { from, to }
  };

  trackRequests[key].messageId = socket.request(name, options, (response) => {
    if (trackRequests[key].messageId === response.id) {
      delete trackRequests[key];
      dispatch(storeWindow({
        key,
        request: payload.request,
        ...response.options
      }));
    }
  });
};

const trackCacheMiddleware = store => next => action => { // eslint-disable-line no-unused-vars
  switch (action.type) {
    case queryCount.type: {
      countTracks(action.payload);
      break;
    }

    case invalidateWindow.type: {
      const key = action.payload.key;
      const state = store.getState();
      const existing = state.tracks[key];
      const request = existing && existing.request;

      if (request) {
        store.dispatch(queryWindow({ invalidate: true, key, request }));
      }

      break;
    }

    case queryWindow.type: {
      const payload = action.payload;
      const key = payload.key;
      const request = payload.request || { };
      const from = request.options.offset || 0;
      const to = from + (request.options.limit || 0);

      const existing =
        _.get(trackRequests, `${key}.range`) ||
        _.get(store, `${key}.range`);

      if (existing && !payload.invalidate) {
        if (existing.from <= from && existing.to >= to) {
          // console.log("already have data");
          return;
        }
        else if (existing.messageId) {
          // console.log("need more data");
          socket.cancel(existing.messageId);
          delete trackRequests[key];
        }
      }

      queryTracks(payload);

      break;
    }

    default: {
      next(action);
      break;
    }
  }
};

export default trackCacheMiddleware;
